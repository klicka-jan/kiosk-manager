#!/bin/bash
rpiname="$1"
if ! [[ "${rpiname}" =~ ^[a-zA-Z0-9]+$ ]]; then
	echo "invalid"
	exit 1
fi

################# input
osimage="raspbian.img"
sddevice="$2"

millis=$(date +%s%3N)
millisFolder="/mnt/kiosk-manager/${millis}"
mntboot="${millisFolder}/sd-boot"
mntdata="${millisFolder}/sd-data"

#################


echo "Flashing image of ${sddevice}"
dd if="${osimage}" of="${sddevice}" bs=1M status='progress'
sync
echo

echo "Mounting partitions to ${mntboot} and ${mntdata}"
mkdir -p "${mntboot}/" "${mntdata}/"
mount "${sddevice}1" "${mntboot}/"
mount "${sddevice}2" "${mntdata}/"
echo

echo "Copying wifi config and enablings ssh"
cp sd-files/wpa_supplicant.conf "${mntboot}/"
touch "${mntboot}/ssh"
# todo forbid password login, change pi password
echo

echo "Copying client installer and server domain"
cp -r "sd-files/kiosk-client/" "${mntdata}/home/pi/kiosk-client/"
echo "$rpiname" > "${mntdata}/home/pi/kiosk-client/identity"

if [ "$#" -eq 3 ]; then
	echo "Copying web url"
	mkdir "${mntdata}/home/pi/kiosk-client/download/"
	echo "$3" > "${mntdata}/home/pi/kiosk-client/download/url"
fi
chown -R 1000:1000 "${mntdata}/home/pi/kiosk-client"
echo

echo "Copying .ssh/authorized_keys"
mkdir "${mntdata}/home/pi/.ssh"
cp "sd-files/ssh/authorized_keys" "${mntdata}/home/pi/.ssh/authorized_keys"
chown -R 1000:1000 "${mntdata}/home/pi/.ssh"
echo

echo "Setting up kiosk update service"
cp "sd-files/systemd/kiosk-client-update.service" "sd-files/systemd/kiosk-client-update.timer" "${mntdata}/etc/systemd/system/"
ln -s "/etc/systemd/system/kiosk-client-update.service" "${mntdata}/etc/systemd/system/multi-user.target.wants/kiosk-client-update.service"
ln -s "/etc/systemd/system/kiosk-client-update.timer" "${mntdata}/etc/systemd/system/timers.target.wants/kiosk-client-update.timer"
echo

echo "Disabling screen blanking"
mkdir "${mntdata}/etc/X11/xorg.conf.d/"
cp "${mntdata}/usr/share/raspi-config/10-blanking.conf" "${mntdata}/etc/X11/xorg.conf.d/"
echo

echo "Removing raspi welcome popup"
rm "${mntdata}/etc/xdg/autostart/piwiz.desktop"
echo

# cleanup
echo "Unmounting and deleting ${mntboot} ${mntdata}"

umount "${mntboot}"
rmdir "${mntboot}"

umount "${mntdata}"
rmdir "${mntdata}"

rmdir "${millisFolder}"
