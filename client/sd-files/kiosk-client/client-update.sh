kioskWd='/home/pi/kiosk-client'
kioskDlWd="${kioskWd}/download"

domain=$(cat "${kioskWd}/server-hostname")
installerEndpoint='api/client/linux/client-installer.sh'
versionEndpoint='api/client/linux/version'

echo "Checking new version"


mkdir -p "${kioskDlWd}/"
curl -f -m 60 -G "${domain}/${versionEndpoint}" -o "${kioskDlWd}/version"

if [ "$?" -ne 0 ]; then
	echo "Failed to get version."
	exit 1
fi

currentVersion=$(cat "${kioskWd}/version")
newVersion=$(cat "${kioskDlWd}/version")

if [  "${currentVersion}" != "${newVersion}" ]; then
	echo "New version available ${newVersion}"
	curl -f -m 60 -G "${domain}/${installerEndpoint}" -o "${kioskDlWd}/client-installer.sh"

	if [ "$?" -ne 0 ]; then
		echo "Failed to get installer."
		exit 1
	fi

	echo "Running installer"
	bash "${kioskDlWd}/client-installer.sh"
else 
	echo "Current version ${currentVersion} is up to date"
fi
