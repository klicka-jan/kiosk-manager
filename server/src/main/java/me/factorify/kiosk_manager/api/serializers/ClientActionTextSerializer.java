package me.factorify.kiosk_manager.api.serializers;

import me.factorify.kiosk_manager.application_logic.client_action.ClientAction;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
 * Serializes actions in text format.
 * Format is:
 * - each line represents one action
 * - parameters of each action are separated by ';'
 */
public class ClientActionTextSerializer {
    private final List<ClientAction> actions;

    public ClientActionTextSerializer(List<ClientAction> actions) {
        this.actions = actions;
    }

    public String serialize() {
        return actions.stream().map(action -> {
            var parts = new ArrayList<String>();
            parts.add(action.getType().toString());
            parts.addAll(action.getParameters());
            return String.join(";", parts);
        }).collect(Collectors.joining("\n"));
    }
}
