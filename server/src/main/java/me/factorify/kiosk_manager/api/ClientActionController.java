package me.factorify.kiosk_manager.api;

import me.factorify.kiosk_manager.api.dto.SaveSSHKeyDTO;
import me.factorify.kiosk_manager.application_logic.KioskService;
import me.factorify.kiosk_manager.application_logic.client_action.ClientAction;
import me.factorify.kiosk_manager.application_logic.client_action.ClientActionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Controller exposing interface for handling kiosk client actions for the administrator.
 * If the request specifies a kiosk, that is not found, a {@link javax.persistence.EntityNotFoundException} (404) will be thrown.
 */
@RestController
@RequestMapping(path = "client")
public class ClientActionController {
    private static final Logger log = LoggerFactory.getLogger(ClientActionController.class);

    private final KioskService kioskService;

    private final ClientActionService clientActionService;

    @Autowired
    public ClientActionController(ClientActionService clientActionService, KioskService kioskService) {
        this.clientActionService = clientActionService;
        this.kioskService = kioskService;
    }

    /**
     * GET "client/action"
     * Gets all actions that are currently requested from all kiosks.
     */
    @GetMapping(path = "/action")
    public Map<String, List<ClientAction>> getPendingActions() {
        log.info("Retrieving all pending actions");
        return clientActionService.getAllActions();
    }

    /**
     * GET "client/action/{kioskName}"
     * Gets actions that are currently requested from specified kiosk, does not modify state.
     */
    @GetMapping(path = "/{kioskId}/action", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<ClientAction> getPendingActionsForKiosk(@PathVariable(name = "kioskId") long kioskId) {
        log.info("Retrieving actions for kiosk {}", kioskId);
        return clientActionService.getActionsFor(kioskService.getByIdOrThrow(kioskId));
    }

    /**
     * POST "client/log/system/{kioskName}"
     * Creates action to retrieve SYSTEM log from specified kiosk.
     */
    @PostMapping(path = "/{kioskId}/log/system")
    public void requestSystemLog(@PathVariable(name = "kioskId") long kioskId) {
        log.info("Requesting system log from kiosk {}", kioskId);
        clientActionService.requestSystemLog(kioskService.getByIdOrThrow(kioskId));
    }

    /**
     * POST "client/restart/{kioskName}"
     * Creates action to restart specified kiosk.
     */
    @PostMapping(path = "/{kioskId}/restart")
    public void requestRestart(@PathVariable(name = "kioskId") long kioskId) {
        log.info("Requesting restart for kiosk {}", kioskId);
        clientActionService.requestRestart(kioskService.getByIdOrThrow(kioskId));
    }

    /**
     * POST "client/ssh"
     * Creates action to upload ssh key to specified kiosks via {@link SaveSSHKeyDTO}.
     */
    @PostMapping(path = "/ssh")
    public void requestSSHKeySave(@RequestBody SaveSSHKeyDTO request) {
        log.info("Requesting uploading ssh key {}", request);
        if (request.isForAllKiosks()) {
            clientActionService.addAction(new ClientAction(ClientAction.Type.SSH_ADD, request.getSshPublicPart()));
        } else {
            clientActionService.addAction(
                    kioskService.getByIds(request.getKioskIds()),
                    new ClientAction(ClientAction.Type.SSH_ADD, request.getSshPublicPart())
            );
        }
    }
}
