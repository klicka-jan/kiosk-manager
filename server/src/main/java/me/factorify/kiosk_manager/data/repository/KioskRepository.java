package me.factorify.kiosk_manager.data.repository;

import me.factorify.kiosk_manager.data.entity.Kiosk;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Repository for handling CRUD operations for Kiosk.
 */
@Repository
public interface KioskRepository {
    Optional<Kiosk> getByName(String name);

    List<Kiosk> getAll();

    Kiosk save(Kiosk kiosk);

    void delete(long id);

    Optional<Kiosk> getById(long id);

    List<Kiosk> getByIds(List<Long> ids);
}
