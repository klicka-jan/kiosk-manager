package me.factorify.kiosk_manager.data.repository;

import io.ebean.Database;
import me.factorify.kiosk_manager.data.entity.ClientConnection;
import me.factorify.kiosk_manager.data.entity.query.QClientConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Ebean ORM implementation of {@link ClientConnectionsRepository}
 */
@Repository
public class EbeanClientConnectionRepository implements ClientConnectionsRepository {
    private final Database database;

    @Autowired
    public EbeanClientConnectionRepository(Database database) {
        this.database = database;
    }

    @Override
    public ClientConnection save(ClientConnection clientConnection) {
        this.database.save(clientConnection);
        return clientConnection;
    }

    @Override
    public Optional<ClientConnection> findByKioskIdAndIp(long id, String ip) {
        return new QClientConnection()
                .kiosk.id.eq(id)
                .ipAddress.eq(ip)
                .findOneOrEmpty();
    }

    @Override
    public List<ClientConnection> findByKioskId(long id) {
        return new QClientConnection()
                .kiosk.id.eq(id)
                .findList();
    }
}
