package me.factorify.kiosk_manager.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Represents last connection made by kiosk from IP address.
 */
@Entity
@Table(name = "client_connections",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"kiosk_id", "ip_address"})})
public class ClientConnection {
    @Id
    private long id;

    @ManyToOne
    @Column(nullable = false)
    @JsonIgnoreProperties({"url", "location", "description"})
    private Kiosk kiosk;

    @Column(nullable = false)
    private String ipAddress;

    @Column(nullable = false)
    private LocalDateTime lastConnectedAt;

    public ClientConnection(Kiosk kiosk, String ipAddress, LocalDateTime lastConnectedAt) {
        this.kiosk = kiosk;
        this.ipAddress = ipAddress;
        this.lastConnectedAt = lastConnectedAt;
    }

    public long getId() {
        return id;
    }

    public Kiosk getKiosk() {
        return kiosk;
    }

    public void setKiosk(Kiosk kiosk) {
        this.kiosk = kiosk;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public LocalDateTime getLastConnectedAt() {
        return lastConnectedAt;
    }

    public void setLastConnectedAt(LocalDateTime lastConnectedAt) {
        this.lastConnectedAt = lastConnectedAt;
    }

    @Override
    public String toString() {
        return "ClientConnection{" +
                "id=" + id +
                ", kiosk=" + kiosk +
                ", ipAddress='" + ipAddress + '\'' +
                ", connectedAt=" + lastConnectedAt +
                '}';
    }
}
