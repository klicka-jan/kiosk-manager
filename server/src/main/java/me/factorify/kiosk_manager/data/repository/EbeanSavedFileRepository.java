package me.factorify.kiosk_manager.data.repository;

import io.ebean.Database;
import me.factorify.kiosk_manager.data.entity.SavedFile;
import me.factorify.kiosk_manager.data.entity.query.QSavedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Ebean ORM implementation of {@link SavedFileRepository}
 */
@Repository
public class EbeanSavedFileRepository implements SavedFileRepository {
    private final Database database;

    @Autowired
    public EbeanSavedFileRepository(Database database) {
        this.database = database;
    }

    @Override
    public Optional<SavedFile> getById(long id) {
        return new QSavedFile().setId(id).findOneOrEmpty();
    }

    @Override
    public List<SavedFile> getFiltered(String kioskName,
                                       SavedFile.Type type) {
        var query = new QSavedFile();
        if (kioskName != null) {
            query = query.kiosk.isNotNull()
                    .kiosk.name.eq(kioskName);
        }

        if (type != null) {
            query = query.type.eq(type);
        }

        return query.findList();
    }

    @Override
    public SavedFile save(SavedFile savedFile) {
        database.save(savedFile);
        return savedFile;
    }

    @Override
    public void delete(long id) {
        new QSavedFile()
                .id.eq(id)
                .delete();
    }
}
