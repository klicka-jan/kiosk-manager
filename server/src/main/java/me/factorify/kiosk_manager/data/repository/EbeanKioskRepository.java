package me.factorify.kiosk_manager.data.repository;

import io.ebean.Database;
import me.factorify.kiosk_manager.application_logic.Validate;
import me.factorify.kiosk_manager.data.entity.Kiosk;
import me.factorify.kiosk_manager.data.entity.query.QKiosk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

/**
 * Ebean ORM implementation of {@link KioskRepository}.
 */
@Repository
public class EbeanKioskRepository implements KioskRepository {
    private final Database database;

    @Autowired
    public EbeanKioskRepository(Database database) {
        this.database = database;
    }

    @Override
    public Optional<Kiosk> getByName(String name) {
        return new QKiosk()
                .name.eq(name)
                .findOneOrEmpty();
    }

    @Override
    public Optional<Kiosk> getById(long id) {
        return new QKiosk()
                .setId(id)
                .findOneOrEmpty();
    }

    @Override
    public List<Kiosk> getByIds(List<Long> ids) {
        return new QKiosk()
                .setIdIn(ids)
                .findList();
    }

    @Override
    public List<Kiosk> getAll() {
        return new QKiosk()
                .findList();
    }

    @Override
    public Kiosk save(Kiosk kiosk) {
        Validate.kioskName(kiosk.getName());
        database.save(kiosk);
        return kiosk;
    }

    @Override
    public void delete(long id) {
        this.database.delete(getById(id).orElseThrow(EntityNotFoundException::new));
    }
}
