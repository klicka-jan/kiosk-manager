package me.factorify.kiosk_manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KioskManagerApplication {
    public static void main(String[] args) {
        SpringApplication.run(KioskManagerApplication.class, args);
    }
}
