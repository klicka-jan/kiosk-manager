kioskWd='/home/pi/kiosk-client'
kioskDlWd="${kioskWd}/download"
kioskUlWd="${kioskWd}/upload"
#chromiumLogPath='/home/pi/chromium/chrome_debug.log'

domain=$(cat "${kioskWd}/server-hostname")
kioskName=$(cat "${kioskWd}/identity")

kioskActionsUrl="${domain}/api/client/linux/action/${kioskName}"
uploadSystemLogUrl="${domain}/api/storage/log/system/${kioskName}"
#uploadBrowserLogUrl="${domain}/api/storage/log/browser/${kioskName}"

echo "Kiosk actions url ${kioskActionsUrl}"
echo "Upload syslog url ${uploadSystemLogUrl}"
#echo "Upload browser url ${uploadBrowserLogUrl}"

templatePath="${kioskWd}/autostart_template"
filledAutostartPath="${kioskWd}/autostart"
autostartPath='/home/pi/.config/lxsession/LXDE-pi/autostart'

function getActions() {
    echo "Getting actions from ${kioskActionsUrl} to dl/actions"
    curl -X POST -H "Accept: text/plain" \
        -s -f "${kioskActionsUrl}" >"${kioskDlWd}/actions"
    echo "$?"
    readarray actions <"${kioskDlWd}/actions"
}

function fillTemplate() {
    newUrl="$(cat ${kioskDlWd}/url)"
    echo "Filling template to ${filledAutostartPath}"
    sed -e "s,{webUrl},${newUrl}," "${templatePath}" >"${filledAutostartPath}" # use ",' instead of '/' in sed because urls contain '/' which will mess with the sed command
}

function setTemplate() {
    echo "Setting template from ${filledAutostartPath} to ${autostartPath}"
    cp "${filledAutostartPath}" "${autostartPath}"
}

function restartUI() {
    echo 'Restarting ui'
    systemctl restart lightdm
}

function addSSH() {
    echo 'Add shh pub'
    echo "${sshPubToAdd}" >> /home/pi/.ssh/authorized_keys
}

function updateView() {
    fillTemplate

    currentTemplate=$(cat "${autostartPath}")
    newTemplate=$(cat "${filledAutostartPath}")

    if [ "${currentTemplate}" = "${newTemplate}" ]; then
        echo "Same template"
    else
        echo "New template"
        setTemplate
        restartUI
    fi
}

function uploadSysLog() {
    echo "Uploading sys log"
    journalctl --no-pager --since yesterday >"${kioskUlWd}/syslog"
    curl -F kiosk_name="${kioskName}" \
        -F file="@${kioskUlWd}/syslog" \
        -s -f "${uploadSystemLogUrl}"
}

# function uploadBrowserLog() {
    # echo "Uploading browser log"
    # cp "${chromiumLogPath}" "${kioskUlWd}/browser-log"
    # curl -F file="@${kioskUlWd}/browser-log" \
        # -s -f "${uploadBrowserLogUrl}"
# }

function processActions() {
    for action in "${actions[@]}"; do
        action=$(tr -d '\n' <<<"$action") #strip new line
        echo "'$action'"
        actionType=$(cut -d ';' -f1 <<<"${action}")
        case "$actionType" in
        URL)
            echo "URL $action"
            newUrl=$(cut -d ';' -f2 <<<"${action}")
            echo "${newUrl}" > "${kioskDlWd}/url"
            updateView
            ;;
        SYSLOG)
            echo "SYSLOG"
            uploadSysLog
            ;;
        # BROWSER_LOG)
            # echo "BROWSERLOG"
            # uploadBrowserLog
            # ;;
        RESTART)
            echo "RESTART"
            reboot
            ;;
        SSH_ADD)
            echo "SSH_ADD"
            sshPubToAdd=$(cut -d ';' -f2 <<<"${action}")
            addSSH
            ;;
        *)
            echo "unknown"
            ;;
        esac
    done
}

getActions
processActions
