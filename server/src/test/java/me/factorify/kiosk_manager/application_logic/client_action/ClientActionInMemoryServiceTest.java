package me.factorify.kiosk_manager.application_logic.client_action;

import me.factorify.kiosk_manager.application_logic.KioskService;
import me.factorify.kiosk_manager.data.entity.Kiosk;
import me.factorify.kiosk_manager.data.repository.ClientConnectionsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ClientActionInMemoryServiceTest {

    private KioskService kioskService = Mockito.mock(KioskService.class);

    private ClientConnectionsRepository clientConnectionsRepository = Mockito.mock(ClientConnectionsRepository.class);

    private ClientActionInMemoryService service = new ClientActionInMemoryService(kioskService, clientConnectionsRepository);

    private Kiosk a = new Kiosk("a", "aa", null, null);
    private Kiosk b = new Kiosk("b", null, null, null);
    private ClientAction url = new ClientAction(ClientAction.Type.URL, "aa");
    private ClientAction restart = new ClientAction(ClientAction.Type.RESTART);
    private ClientAction log = new ClientAction(ClientAction.Type.SYSLOG);
    private ClientAction ssh = new ClientAction(ClientAction.Type.SSH_ADD, "ssh");

    @BeforeEach
    private void setup() {
        service.clearActions();
    }

    @Test
    void register() {
        var aa = new TestKiosk("a", "aa", null, null);
        BDDMockito.given(kioskService.getByName("a")).willReturn(Optional.empty());
        BDDMockito.given(kioskService.save(aa)).willReturn(aa);
        assertEquals(aa, service.register("a", "aa"));
    }

    @Test
    void restartIllegalArgument() {
        assertThrows(IllegalArgumentException.class,
                () -> service.register("", ""));
        assertThrows(IllegalArgumentException.class,
                () -> service.register("@", ""));
        assertThrows(IllegalArgumentException.class,
                () -> service.register("a!", ""));
        assertThrows(IllegalArgumentException.class,
                () -> service.register("#asdf4", ""));
        assertThrows(IllegalArgumentException.class,
                () -> service.register("789[8798", ""));
        assertThrows(IllegalArgumentException.class,
                () -> service.register("askdljf21093 ", ""));
        assertThrows(IllegalArgumentException.class,
                () -> service.register("asdf\n", ""));
        assertThrows(IllegalArgumentException.class,
                () -> service.register("][p[]123", ""));
        assertThrows(IllegalArgumentException.class,
                () -> service.register(";", ""));
        assertThrows(IllegalArgumentException.class,
                () -> service.register("aaa_aa", ""));
        assertThrows(IllegalArgumentException.class,
                () -> service.register("231-", ""));
    }

    @Test
    void registerExisting() {
        BDDMockito.given(kioskService.getByName("b")).willReturn(Optional.of(b));
        assertEquals(b, service.register("b", "bb"));
    }

    @Test
    void getAllActions() {
        service.addAction(a, log);
        service.addAction(b, log);
        service.addAction(a, restart);

        assertEquals(
                Map.of("a", List.of(log, restart),
                        "b", List.of(log)),
                service.getAllActions()
        );
    }


    @Test
    void clear() {
        service.addAction(a, log);
        assertEquals(1, service.getActionsFor(a).size());
        service.clearActions();
        assertEquals(0, service.getActionsFor(a).size());
    }

    @Test
    void processActions() {
        service.addAction(a, log);
        service.addAction(b, log);
        service.addAction(a, restart);

        assertEquals(List.of(url, log, restart), service.processActions(a));

        assertEquals(
                Map.of("b", List.of(log)),
                service.getAllActions()
        );
    }

    @Test
    void getActionsForOne() {
        service.addAction(a, log);
        service.addAction(b, log);
        service.addAction(a, restart);

        assertEquals(List.of(log, restart), service.getActionsFor(a));
        assertEquals(List.of(log), service.getActionsFor(b));
        assertEquals(List.of(), service.getActionsFor(new Kiosk("c", null, null, null)));
    }

    @Test
    void addActionForMultiple() {
        service.addAction(List.of(a, b), log);

        assertEquals(List.of(log), service.getActionsFor(a));
        assertEquals(List.of(log), service.getActionsFor(b));
    }

    @Test
    void testAddAction() {
        BDDMockito.when(kioskService.getAll()).thenReturn(List.of(a, b));
        service.addAction(log);

        assertEquals(List.of(log), service.getActionsFor(a));
        assertEquals(List.of(log), service.getActionsFor(b));
    }
}
