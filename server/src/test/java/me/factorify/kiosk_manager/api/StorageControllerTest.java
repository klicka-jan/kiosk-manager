package me.factorify.kiosk_manager.api;

import me.factorify.kiosk_manager.api.exception.ControllerExceptionHandler;
import me.factorify.kiosk_manager.application_logic.KioskService;
import me.factorify.kiosk_manager.application_logic.storage.StorageService;
import me.factorify.kiosk_manager.data.entity.Kiosk;
import me.factorify.kiosk_manager.data.entity.SavedFile;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

class StorageControllerTest {
    private StorageService storageService = Mockito.mock(StorageService.class);

    private KioskService kioskService = Mockito.mock(KioskService.class);

    private MockMvc mockMvc = MockMvcBuilders
            .standaloneSetup(new StorageController(storageService, kioskService))
            .setControllerAdvice(new ControllerExceptionHandler())
            .build();

    @Test
    void uploadBrowserLog() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "browserlog.txt", "text/plain", "log".getBytes());
        var kiosk = new Kiosk("a", null, null, null);
        BDDMockito.given(storageService.saveBrowserLog(ArgumentMatchers.any(), ArgumentMatchers.any())).willReturn(new SavedFile("file", "file", kiosk, SavedFile.Type.BROWSER_LOG));
        var a = mockMvc.perform(
                MockMvcRequestBuilders.multipart("/storage/log/browser/a")
                        .file(file)
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.type", CoreMatchers.is("BROWSER_LOG")))
                .andExpect(jsonPath("$.name", CoreMatchers.is("file")));
    }

    @Test
    void uploadBrowserLogFails() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "browserlog.txt", "text/plain", "log".getBytes());
        var kiosk = new Kiosk("a", null, null, null);
        BDDMockito.given(storageService.saveBrowserLog(ArgumentMatchers.any(), ArgumentMatchers.any())).willThrow(IOException.class);
        mockMvc.perform(
                MockMvcRequestBuilders.multipart("/storage/log/browser/a")
                        .file(file)
        ).andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    @Test
    void uploadSysLog() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "browserlog.txt", "text/plain", "log".getBytes());
        var kiosk = new Kiosk("a", null, null, null);
        BDDMockito.given(storageService.saveSystemLog(ArgumentMatchers.any(), ArgumentMatchers.any())).willReturn(new SavedFile("file", "file", kiosk, SavedFile.Type.SYSTEM_LOG));
        var a = mockMvc.perform(
                MockMvcRequestBuilders.multipart("/storage/log/system/a")
                        .file(file)
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.type", CoreMatchers.is("SYSTEM_LOG")))
                .andExpect(jsonPath("$.name", CoreMatchers.is("file")));
    }

    @Test
    void uploadSysLogFails() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "browserlog.txt", "text/plain", "log".getBytes());
        var kiosk = new Kiosk("a", null, null, null);
        BDDMockito.given(storageService.saveSystemLog(ArgumentMatchers.any(), ArgumentMatchers.any())).willThrow(IOException.class);
        mockMvc.perform(
                MockMvcRequestBuilders.multipart("/storage/log/system/a")
                        .file(file)
        ).andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    @Test
    void getById() throws Exception {
        BDDMockito.given(storageService.getByIdOrThrow(1)).willReturn(new SavedFile("a", "a", null, null));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/storage/1")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    void getByIdFails() throws Exception {
        BDDMockito.given(storageService.getByIdOrThrow(1)).willThrow(EntityNotFoundException.class);
        mockMvc.perform(
                MockMvcRequestBuilders.get("/storage/1")
        ).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void getFiltered() throws Exception {
        BDDMockito.given(storageService.getFiltered("aa", SavedFile.Type.BROWSER_LOG)).willReturn(List.of());
        mockMvc.perform(
                MockMvcRequestBuilders.get("/storage")
                .queryParam("kiosk_name", "aa")
                .queryParam("file_type", "SYSTEM_LOG")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }
}
