package me.factorify.kiosk_manager.api;

import me.factorify.kiosk_manager.api.dto.SaveSSHKeyDTO;
import me.factorify.kiosk_manager.api.exception.ControllerExceptionHandler;
import me.factorify.kiosk_manager.application_logic.KioskService;
import me.factorify.kiosk_manager.application_logic.client_action.ClientAction;
import me.factorify.kiosk_manager.application_logic.client_action.ClientActionService;
import me.factorify.kiosk_manager.data.entity.Kiosk;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Map;

import static me.factorify.kiosk_manager.TestUtils.toJson;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

class ClientActionControllerTest {
    private ClientActionService clientActionService = Mockito.mock(ClientActionService.class);

    private KioskService kioskService = Mockito.mock(KioskService.class);

    private MockMvc mockMvc = MockMvcBuilders
            .standaloneSetup(new ClientActionController(clientActionService, kioskService))
            .setControllerAdvice(new ControllerExceptionHandler())
            .build();

    private static final Map<String, List<ClientAction>> allActions = Map.of(
            "a", List.of(
                    new ClientAction(ClientAction.Type.URL, "www.google.com")
            ),
            "b", List.of(
                    new ClientAction(ClientAction.Type.SYSLOG),
                    new ClientAction(ClientAction.Type.RESTART)
            ));

    private static final Kiosk a = new Kiosk("a", "www.google.com", null, null);
    private static final List<ClientAction> aActions = allActions.get("a");

    private static final Kiosk b = new Kiosk("b", null, null, null);
    private static final List<ClientAction> bActions = allActions.get("b");


    @BeforeEach
    public void setup() {
        BDDMockito.given(kioskService.getByNameOrThrow("a")).willReturn(a);
        BDDMockito.given(kioskService.getByIdOrThrow(1)).willReturn(a);
        BDDMockito.given(kioskService.getByNameOrThrow("b")).willReturn(b);
        BDDMockito.given(kioskService.getByIdOrThrow(2)).willReturn(b);
        BDDMockito.given(kioskService.getByNameOrThrow("c")).willThrow(EntityNotFoundException.class);
        BDDMockito.given(kioskService.getByIdOrThrow(3)).willThrow(EntityNotFoundException.class);
    }


    @Test
    void getPendingActions() throws Exception {
        BDDMockito.given(clientActionService.getAllActions()).willReturn(allActions);
        mockMvc.perform(
                        MockMvcRequestBuilders.get("/client/action")
                ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.a.[0].type", CoreMatchers.is("URL")))
                .andExpect(jsonPath("$.a.[0].parameters.[0]", CoreMatchers.is("www.google.com")))
                .andExpect(jsonPath("$.b.[0].type", CoreMatchers.is("SYSLOG")))
                .andExpect(jsonPath("$.b.[1].type", CoreMatchers.is("RESTART")));
    }

    @Test
    void getPendingActionsForKiosk() throws Exception {
        BDDMockito.given(clientActionService.getActionsFor(a)).willReturn(aActions);
        mockMvc.perform(
                        MockMvcRequestBuilders.get("/client/1/action")
                ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.[0].type", CoreMatchers.is("URL")))
                .andExpect(jsonPath("$.[0].parameters.[0]", CoreMatchers.is("www.google.com")));
    }

    @Test
    void getPendingActionsForKioskNotFound() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/client/3/action")
        ).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    //    @Test
    //    void requestBrowserLog() throws Exception {
    //        mockMvc.perform(
    //                MockMvcRequestBuilders.post("/client/log/browser/a")
    //        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    //    }
    //
    //    @Test
    //    void requestBrowserLogNotFound() throws Exception {
    //        mockMvc.perform(
    //                MockMvcRequestBuilders.post("/client/log/browser/c")
    //        ).andExpect(MockMvcResultMatchers.status().isNotFound());
    //    }

    @Test
    void requestSystemLog() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/client/1/log/system")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    void requestSystemLogNotFound() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/client/3/log/system")
        ).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void requestRestart() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/client/1/restart")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    void requestRestartNotFound() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/client/3/restart")
        ).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void requestSSHKeySaveForSpecificKiosks() throws Exception {
        BDDMockito.when(kioskService.getByIds(List.of(1L))).thenReturn(List.of(a));
        mockMvc.perform(
                MockMvcRequestBuilders.post("/client/ssh")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(new SaveSSHKeyDTO("ssh", List.of(1L))))
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void requestSSHKeySaveForAll() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/client/ssh")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(new SaveSSHKeyDTO("ssh", true)))
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }
}
