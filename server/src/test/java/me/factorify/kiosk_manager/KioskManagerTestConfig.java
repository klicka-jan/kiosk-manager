package me.factorify.kiosk_manager;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Configures Spring autowiring and DB access for tests if needed.
 */
@SpringBootTest
@AutoConfigureMockMvc
@EntityScan({"me.factorify.kiosk_manager"})
public class KioskManagerTestConfig {
}
